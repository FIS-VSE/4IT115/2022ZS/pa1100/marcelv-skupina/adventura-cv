package cz.vse.java.xname.adventuracv.gui;

import cz.vse.java.xname.adventuracv.logika.HerniPlan;
import cz.vse.java.xname.adventuracv.logika.Hra;
import cz.vse.java.xname.adventuracv.logika.Prostor;
import cz.vse.java.xname.adventuracv.observer.Observer;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.util.Callback;

import java.util.Collection;
import java.util.Set;

public class PanelVychodu implements Observer {

    private ObservableList<String> observableList = FXCollections.observableArrayList();
    private ListView<String> listView = new ListView<>(observableList);
    private HerniPlan herniPlan;

    public PanelVychodu(Hra hra) {

        herniPlan = hra.getHerniPlan();
        herniPlan.register(this);

        listView.setMaxWidth(250);

        listView.setCellFactory(stringListView -> new ListCell<String>() {
            @Override
            protected void updateItem(String nazev, boolean empty) {
                super.updateItem(nazev, empty);
                if(!empty) {
                    setText(nazev);
                    ImageView iw = new ImageView("/Troll-face.sh.png");
                    iw.setPreserveRatio(true);
                    iw.setFitWidth(90);
                    setGraphic(iw);
                } else {
                    setText(null);
                    setGraphic(null);
                }
            }
        });

        nastavVychody();
    }

    private void nastavVychody() {
        observableList.clear();
        Collection<Prostor> vychody = herniPlan.getAktualniProstor().getVychody();
        for (Prostor vychod : vychody) {
            observableList.add(vychod.getNazev());
        }
    }

    public ListView<String> getListView() {
        return listView;
    }

    @Override
    public void update() {
        nastavVychody();
    }
}
