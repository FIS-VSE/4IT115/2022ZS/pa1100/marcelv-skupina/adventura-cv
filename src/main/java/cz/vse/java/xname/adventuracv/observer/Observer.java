package cz.vse.java.xname.adventuracv.observer;

public interface Observer {

    void update();

}
