package cz.vse.java.xname.adventuracv.observer;

public interface SubjectOfChange {

    void register(Observer observer);

    void notifyObservers();

    void unregister(Observer observer);

}
